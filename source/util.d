module util;

int firstGreaterPowerOfTwo(int value)
{
    if (value < 2) {
        return value;
    }
    value = (--value | value >> 1);
    value |= value >> 2;
    value |= value >> 4;
    value |= value >> 8;
    value |= value >> 16;
    return ++value;
}

int getIntFromTwoInt(int a, int b)
{
    return a << 16 | (b & 0xFFFF);
}

immutable(ubyte[]) slurpFile(string path)
{
    import std.stdio;
    import std.exception: assumeUnique;

    auto file = File(path);
    file.seek(0, SEEK_END);
    auto length = file.tell();
    file.seek(0, SEEK_SET);
    auto buffer = new ubyte[length];
    file.rawRead(buffer);
    file.close();
    return assumeUnique(buffer);
}

auto parseFileWith(alias reader)(string path)
{
    import io.data:BinaryDataReader;

    auto bytes = slurpFile(path);
    return reader(BinaryDataReader(bytes));
}
