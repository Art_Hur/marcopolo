import map.environment;
import map.gfx;
import map.partition;
import map.topology;
import map.world_element;
import map.world_group;
import map.world_info;
import util;

void saveToFile(T)(string name, auto ref T t) {
    import std.stdio: File, write;

    auto file = File("./out/" ~ name, "w");
    file.write(t);
    file.close();
}

int main()
{
    auto elements = parseFileWith!readElementProperties("./data/elements.lib");
    saveToFile("elements.txt", elements);
    auto worlds = parseFileWith!readWorldInfos("./data/worlds.lib");
    saveToFile("worlds.txt", worlds);
    auto gfxCoords = parseFileWith!readPartition("./data/gfx/coord");
    saveToFile("gfx_coords.txt", gfxCoords);
    auto gfx = parseFileWith!readGfx("./data/gfx/11_-9");
    saveToFile("gfx.txt", gfx);
    auto groups = parseFileWith!readWorldGroup("./data/gfx/groups.lib");
    saveToFile("groups.txt", groups);
    auto tplgCoords = parseFileWith!readPartition("./data/tplg/coord");
    saveToFile("tplg_coords.txt", tplgCoords);
    auto tplg = parseFileWith!readTopology("./data/tplg/12_1");
    saveToFile("tplg.txt", tplg);
    auto env = parseFileWith!readEnvironment("./data/env/-3_39");
    saveToFile("env.txt", env);
    return 0;
}
