module io.data;
import std.array: uninitializedArray;
import std.traits: isScalarType, isIntegral;
import internal.endian;

struct BinaryDataReader {
    this(immutable(ubyte[]) buf)
    {
        _buffer = buf;
    }

    bool readBoolBit() pure nothrow @nogc @trusted
    {
        if (_position == _lastBooleanBitOffset && _lastBooleanBitIndex < 7) {
            _lastBooleanBitIndex++;
            return (_lastBooleanBitField & 1 << 7 - _lastBooleanBitIndex) != 0;
        }
        _lastBooleanBitIndex = 0;
        _lastBooleanBitOffset = _position + 1;
        _lastBooleanBitField = read!ubyte;
        return (_lastBooleanBitField & 0x80) != 0;
    }

    T read(T : bool)() pure nothrow @nogc @trusted
    {
        return cast(bool) read!ubyte;
    }

    T read(T)() pure nothrow @nogc @trusted if (isScalarType!T)
    {
        assert(_position + T.sizeof <= _buffer.length, "attempt to read past stream");
        T v = *cast(T *) &_buffer[_position];
        _position += T.sizeof;
        static if (isNative!(T, Endian.littleEndian))
            return v;
        else return byteswap(v);
    }

    string readString(P)() pure nothrow @nogc @trusted
    {
        immutable length = read!P;
        auto buf = cast(string) _buffer[_position .. _position + length];
        _position += length;
        return buf;
    }

    string readCString() pure nothrow @nogc @trusted
    {
        immutable start = _position;
        while (read!ubyte != 0) { }
        string str = cast(string) _buffer[start .. _position - 1];
        return str;
    }

    T[] readArray(T)(size_t size) @trusted
    if (isScalarType!T && T.sizeof == 1)
    {
        auto buf = cast(T[]) _buffer[_position .. _position + size].dup;
        _position += size;
        return buf;
    }

    T[] readArray(T)(size_t size) @trusted
    if (isScalarType!T && T.sizeof > 1)
    {
        auto arr = uninitializedArray!(T[])(size);
        for (size_t i = 0; i < size; i++)
            arr[i] = read!T;
        return arr;
    }

    T[] readArray(P, T)() @trusted if (isIntegral!P)
    {
        immutable length = read!P;
        return readArray!T(length);
    }

    size_t remaining() pure nothrow @property @nogc @trusted
    {
        return _buffer.length - _position;
    }

private:
    int _position = 0;
    immutable(ubyte[]) _buffer;

    int _lastBooleanBitOffset = -1;
    int _lastBooleanBitIndex = -1;
    ubyte _lastBooleanBitField = 0;
}

auto readArrayWith(P, alias read)(auto ref BinaryDataReader stream) @trusted
if (isIntegral!P)
{
    alias T = typeof(read(stream));
    immutable length = stream.read!P;
    auto arr = uninitializedArray!(T[])(length);
    for (size_t i = 0; i < length; i++)
        arr[i] = read(stream);
    return arr;
}
