module map.gfx;
import io.data;
import std.array: appender;

struct ScreenElement {
    short cellZ;
    byte height;
    byte altitudeOrder;
    bool occluder;
    byte type;
    int elementId;

    static ScreenElement read()(auto ref BinaryDataReader stream) {
        auto cellZ = stream.read!short,
          height = stream.read!byte,
          altitudeOrder = stream.read!byte,
          occluder = stream.readBoolBit;
        auto type = cast(byte) stream.readBoolBit;
        type |= (stream.readBoolBit ? 2 : 0);
        type |= (stream.readBoolBit ? 4 : 0);
        auto elementId = stream.read!int;

        return ScreenElement(cellZ, height, altitudeOrder, occluder, type, elementId);
    }
}

struct RenderElement {
    ScreenElement base;
    alias base this;

    int groupKey;
    byte layerIdx;
    int groupId;
    float[] colors;
}

RenderElement[] readGfx()(auto ref BinaryDataReader stream)
{
    auto coordMinX = stream.read!int,
      coordMinY = stream.read!int,
      coordMinZ = stream.read!short,
      coordMaxX = stream.read!int,
      coordMaxY = stream.read!int,
      coordMaxZ = stream.read!short,
      cnt = stream.read!ushort,
      groupKeys = new int[cnt],
      layerIndexes = new byte[cnt],
      groupIds = new int[cnt];

    for (int i = 0; i < cnt; i++) {
        groupKeys[i] = stream.read!int;
        layerIndexes[i] = stream.read!byte;
        groupIds[i] = stream.read!int;
    }
    auto colors = readColorTable(stream);
    immutable
      mapX = stream.read!int,
      mapY = stream.read!int,
      rectCount = stream.read!ushort;
    auto elems = appender!(RenderElement[])();
    elems.reserve(rectCount * 10);
    for (int i = 0; i < rectCount; i++) {
        immutable
          minX = mapX + stream.read!ubyte,
          maxX = mapX + stream.read!ubyte,
          minY = mapY + stream.read!ubyte,
          maxY = mapY + stream.read!ubyte;
        for (int cx = minX; cx < maxX; cx++) {
            for (int cy = minY; cy < maxY; cy++) {
                for (auto elemCount = stream.read!ubyte, j = 0; j < elemCount; j++) {
                    auto element = ScreenElement.read(stream),
                      groupIdx = stream.read!ushort,
                      groupKey = groupKeys[groupIdx],
                      layerIdx = layerIndexes[groupIdx],
                      groupId = groupIds[groupIdx],
                      colorIdx = stream.read!ushort,
                      clrs = colors[colorIdx];
                    elems ~= RenderElement(element, groupKey, layerIdx, groupId, clrs);
                }
            }
        }
    }
    return elems.data;
}

private {
    float[][] readColorTable(ref BinaryDataReader stream)
    {
        int colorCount = stream.read!ushort;
        auto colors = new float[][] (colorCount, 0);
        for (int i = 0; i < colorCount; ++i) {
            byte type = stream.read!byte;
            readColors(colors[i] = newColorsFromType(type), type, stream);
        }
        return colors;
    }

    float getTeintValue(byte value)
    {
        return value / 255.0f + 0.5f;
    }

    void readColors(ref float[] colors, int type, ref BinaryDataReader stream)
    {
        int i = 0;
        if ((type & 0x1) == 0x1) {
            colors[i++] = getTeintValue(stream.read!byte);
            colors[i++] = getTeintValue(stream.read!byte);
            colors[i++] = getTeintValue(stream.read!byte);
        }
        if ((type & 0x2) == 0x2) {
            colors[i++] = stream.read!byte / 255.0f + 0.5f;
        }
        if ((type & 0x4) == 0x4) {
            if ((type & 0x1) == 0x1) {
                colors[i++] = stream.read!byte / 255.0f + 0.5f;
                colors[i++] = stream.read!byte / 255.0f + 0.5f;
                colors[i++] = stream.read!byte / 255.0f + 0.5f;
            }
            if ((type & 0x2) == 0x2) {
                colors[i++] = stream.read!byte / 255.0f + 0.5f;
            }
        }
    }

    float[] newColorsFromType(int type)
    {
        int size = 0;
        size += (((type & 0x1) == 0x1) ? 3 : 0);
        size += (((type & 0x2) == 0x2) ? 1 : 0);
        size *= (((type & 0x4) == 0x4) ? 2 : 1);
        return new float[size];
    }
}
