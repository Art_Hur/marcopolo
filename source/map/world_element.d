module map.world_element;
import io.data;
import util: firstGreaterPowerOfTwo;

struct WorldElement {
    int id;
    short originX;
    short originY;
    short imgWidth;
    short imgHeight;
    int gfxId;
    byte propertiesFlag;
    byte visualHeight;
    byte visibilityMask;
    byte exportMask;
    byte shader;
    AnimData animData;
    TextureCoords textureCoords;
    byte groundSoundType;

    static WorldElement read()(auto ref BinaryDataReader stream)
    {
        auto
          id = stream.read!int,
          originX = stream.read!short,
          originY = stream.read!short,
          imgWidth = stream.read!short,
          imgHeight = stream.read!short,
          gfxId = stream.read!int,
          propertiesFlag = stream.read!byte,
          visualHeight = stream.read!byte,
          visibilityMask = stream.read!byte,
          exportMask = stream.read!byte,
          shader = stream.read!byte,
          flip = (propertiesFlag & 0x10) == 0x10,
          animData = AnimData.read(stream, flip),
          textureCoords = computeTextureCoords(imgWidth, imgHeight, flip),
          groundSoundType = stream.read!byte;

        return WorldElement(id, originX, originY, imgWidth, imgHeight, gfxId, propertiesFlag,
                 visualHeight, visibilityMask, exportMask, shader,
                 animData, textureCoords, groundSoundType);
    }
}

struct AnimData {
    int totalTime;
    short[] animationTimes;
    TextureCoords[] textureCoords;

    static AnimData read()(auto ref BinaryDataReader stream, bool flip)
    {
        auto animCount = stream.read!ubyte;
        if (animCount == 0) return AnimData();
        auto totalTime = stream.read!int,
          imgWidth = stream.read!short,
          imgHeight = stream.read!short,
          imgWidthTotal = stream.read!short,
          imgHeightTotal = stream.read!short,
          animationTimes = stream.readArray!short (animCount),
          textureCoords = stream.readArray!short (animCount * 2);
        return AnimData(totalTime, animationTimes,
                 computeTextureCoords(textureCoords, imgWidth, imgHeight, imgWidthTotal, imgHeightTotal, flip));
    }
}

struct TextureCoords {
    float left;
    float bottom;
    float right;
    float top;
}

WorldElement[] readElementProperties()(auto ref BinaryDataReader stream)
{
    return stream.readArrayWith!(int, WorldElement.read);
}

private {
    TextureCoords computeTextureCoords(int imgWidth, int imgHeight, bool flip)
    {
        float width = firstGreaterPowerOfTwo(imgWidth);
        float height = firstGreaterPowerOfTwo(imgHeight) - 0.5f;
        float right = imgWidth / width;
        float bottom = imgHeight / height;
        if (flip) {
            return TextureCoords(right, bottom, 0.0f, 0.0f);
        }
        return TextureCoords(0.0f, bottom, right, 0.0f);
    }

    TextureCoords[] computeTextureCoords(short[] textureCoords, short imgWidth,
      short imgHeight, int imgWidthTotal,
      int imgHeightTotal, bool flip)
    {
        float widthTotal = firstGreaterPowerOfTwo(imgWidthTotal);
        float heightTotal = firstGreaterPowerOfTwo(imgHeightTotal) - 0.5f;
        float right = imgWidth / widthTotal;
        float bottom = imgHeight / heightTotal;
        auto result = new TextureCoords[textureCoords.length / 2];
        for (int i = 0; i < result.length; ++i) {
            float offsetX = (textureCoords[i * 2] + 0.5f) / widthTotal;
            float offsetY = (textureCoords[i * 2 + 1] + 0.5f) / heightTotal;
            if (flip) {
                result[i] = TextureCoords(right + offsetX, bottom + offsetY, offsetX, offsetY);
            } else {
                result[i] = TextureCoords(offsetX, bottom + offsetY, right + offsetX, offsetY);
            }
        }
        return result;
    }
}
