module map.world_group;
import io.data;

struct Layer {
    int id;
    short[] layersVisible;

    static Layer read()(auto ref BinaryDataReader stream) {
        return Layer(stream.read!short, stream.readArray!(short, short));
    }
}

Layer[] readWorldGroup()(auto ref BinaryDataReader stream) {
    auto layerCount = 1 + stream.read!ushort;
    return stream.readArrayWith!(ushort, Layer.read);
}
