module map.environment;
import io.data;
import std.algorithm.mutation: move;
import std.variant: Algebraic;


struct Element {
    byte x;
    byte y;
    short z;

    static Element read()(auto ref BinaryDataReader stream)
    {
        immutable
          x = stream.read!byte,
          y = stream.read!byte,
          z = stream.read!short;

        return Element(x, y, z);
    }
}

struct Placement {
    int x;
    int y;
    int z;
    byte direction;

    static Placement read()(auto ref BinaryDataReader stream)
    {
        immutable
          x = stream.read!int,
          y = stream.read!int,
          z = stream.read!int,
          direction = stream.read!byte;

        return Placement(x, y, z, direction);
    }
}

struct Particle {
    Element element;
    alias element this;

    int systemId;
    byte level;
    byte offsetX;
    byte offsetY;
    byte offsetZ;
    byte lod;

    static Particle read()(auto ref BinaryDataReader stream)
    {
        immutable
          element = Element.read(stream),
          systemId = stream.read!int,
          level = stream.read!byte,
          offsetX = stream.read!byte,
          offsetY = stream.read!byte,
          offsetZ = stream.read!byte,
          lod = stream.read!byte;

        return Particle(element, systemId, level, offsetX, offsetY, offsetZ, lod);
    }
}

struct Sound {
    Element element;
    alias element this;

    int soundId;

    static Sound read()(auto ref BinaryDataReader stream)
    {
        return Sound(Element.read(stream), stream.read!int);
    }
}

struct Ambiance {
    int[] ambianceIds;
    ubyte[] ambiances;

    static Ambiance read()(auto ref BinaryDataReader stream) {
        immutable idCount = stream.read!ubyte;
        if (idCount == 0) return Ambiance();
        auto ambianceIds = stream.readArray!int (idCount),
          ambiances = stream.readArray!(ubyte, ubyte);
        return Ambiance(ambianceIds, ambiances);
    }
}

struct InteractiveElement {
    long id;
    short type;
    int[] views;
    ubyte[] data;
    bool clientOnly;
    short landMarkType;

    static InteractiveElement read()(auto ref BinaryDataReader stream)
    {
        auto
          id = stream.read!long,
          type = stream.read!short,
          views = stream.readArray!(ubyte, int),
          data = stream.readArray!(ushort, ubyte),
          clientOnly = stream.readBoolBit,
          landMarkType = stream.read!short;

        return InteractiveElement(id, type, views, data, clientOnly, landMarkType);
    }
}

struct DynamicElement {
    Element element;
    alias element this;

    int id;
    int gfxId;
    short type;
    byte direction;
    bool occluder;
    byte height;
    string baseAnimation;
    string params;

    static DynamicElement read()(auto ref BinaryDataReader stream)
    {
        immutable
          element = Element.read(stream),
          id = stream.read!int,
          gfxId = stream.read!int,
          type = stream.read!short,
          direction = stream.read!byte,
          occluder = stream.readBoolBit,
          height = stream.read!byte,
          baseAnimation = stream.readCString,
          params = stream.readCString;

        return DynamicElement(element, id, gfxId, type, direction, occluder, height, baseAnimation, params);
    }
}

struct CellData {
    short[] grounds;
    ubyte[] cells;

    static CellData read()(auto ref BinaryDataReader stream)
    {
        auto cnt = stream.read!ubyte;

        if (cnt == 0)
            return CellData();

        auto grounds = stream.readArray!short (cnt);
        if (cnt != 1) {
            return CellData(move(grounds), stream.readArray!(ubyte, ubyte));
        } else {
            return CellData(move(grounds), []);
        }
    }

    short getValueForCell(int cellIndex)
    {
        auto count = grounds.length;
        if (count == 1) {
            return grounds[0];
        }
        if (count <= 16) {
            int valueByByte = 2;
            int mask = 15;
            if (count <= 4) {
                valueByByte = 4;
                mask = 3;
            }
            if (count <= 2) {
                valueByByte = 8;
                mask = 1;
            }
            int shift = cellIndex % valueByByte * (8 / valueByByte);
            int index = (cells[cellIndex / valueByByte] & 0xFF) >>> shift & mask;
            return grounds[index];
        }
        return 0;
    }
}

alias BagPlacement = Algebraic!(InWorldPlacement, InMarketPlacement, NullPlacement);

struct SterylCells {
    ubyte[] cells;
    int[] cellAltitude;

    static SterylCells read()(auto ref BinaryDataReader stream)
    {
        auto cnt = stream.read!short;

        if (cnt == -1) return SterylCells();
        auto cells = stream.readArray!ubyte (40);

        if (cnt == 0) {
            SterylCells c;
            c.cells = move(cells);
            return c;
        }
        return SterylCells(move(cells), stream.readArray!int (cnt));
    }

    bool isSteryl(byte x, byte y, short z)
    {
        import std.algorithm.searching : canFind;

        assert(x >= 0 && x < 18);
        assert(y >= 0 && y < 18);
        int index = x + y * 18;
        int mask = 128 >> index % 8;
        bool steryl = (cells[index / 8] & mask) != 0x0;
        if (cellAltitude.length == 0 || !steryl) {
            return steryl;
        }
        int hash = x | y << 8 | z << 16;
        return cellAltitude.canFind(hash);
    }
}

struct InWorldPlacement {
    SterylCells sterylCells;

    static InWorldPlacement read()(auto ref BinaryDataReader stream)
    {
        return InWorldPlacement(SterylCells.read(stream));
    }
}

struct InMarketPlacement {
    Placement[] placements;

    static InMarketPlacement read()(auto ref BinaryDataReader stream)
    {
        return InMarketPlacement(stream.readArrayWith!(ubyte, Placement.read));
    }
}

struct NullPlacement { }

BagPlacement readBagPlacement()(auto ref BinaryDataReader stream)
{
    immutable type = stream.read!byte;

    switch (type) {
        case 2:
            return BagPlacement(InWorldPlacement.read(stream));
        case 1:
            return BagPlacement(InMarketPlacement.read(stream));
        default:
            return BagPlacement(NullPlacement());
    }
}

struct MapEnv {
    byte header;
    short x;
    short y;
    Particle[] particles;
    Sound[] sounds;
    Ambiance ambiance;
    InteractiveElement[] ielements;
    DynamicElement[] delements;
    CellData cellData;
    float seaPercent;
    long riverDistribution;
    long lakeDistribution;
    long lavaDistribution;
    long runningLavaDistribution;
    BagPlacement bagPlacement;

    int getAmbianceZone(int cellX, int cellY)
    {
        if (ambiance.ambianceIds.length == 0) {
            return -1;
        }
        if (ambiance.ambiances.length == 0) {
            return ambiance.ambianceIds[0];
        }
        cellX -= x * 18;
        cellY -= y * 18;
        assert(cellX >= 0 && cellX < 18);
        assert(cellY >= 0 && cellY < 18);
        int cellIndex = cellX + cellY * 18;
        int index = ambiance.ambiances[cellIndex / 4] >>> cellIndex % 4 * 2;
        return ambiance.ambianceIds[index & 0x3];
    }
}

MapEnv readEnvironment()(auto ref BinaryDataReader stream)
{
    auto header = stream.read!byte,
      x = stream.read!short,
      y = stream.read!short,
      particles = stream.readArrayWith!(ubyte, Particle.read),
      sounds = stream.readArrayWith!(ubyte, Sound.read),
      ambiance = Ambiance.read(stream),
      ielements = stream.readArrayWith!(ubyte, InteractiveElement.read),
      delements = stream.readArrayWith!(ubyte, DynamicElement.read),
      cellData = CellData.read(stream),
      seaPercent = stream.read!byte / 100.0f,
      riverDistribution = stream.read!long,
      lakeDistribution = stream.read!long,
      lavaDistribution = stream.read!long,
      runningLavaDistribution = stream.read!long,
      bagPlacement = stream.readBagPlacement;

    return MapEnv(header, x, y, particles, sounds, ambiance, ielements, delements, cellData, seaPercent,
             riverDistribution, lakeDistribution, lavaDistribution, runningLavaDistribution, bagPlacement);
}
