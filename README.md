# Dependencies
DUB and LDC compiler

# How to build
```sh
dub build --compiler=ldc
```

# How to run
```sh
mkdir out
./wakfu-map-reader
```